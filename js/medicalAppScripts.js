$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement : 'auto',
        trigger: 'hover'
    });

    $('#showReports').click(function(){
        $('#reportView').removeClass('d-none');
    });
});
